from django.contrib import admin
from .models import swablogdata
from .models import swabpatientdata
from .models import plasmalogdata
from .models import plasmapatientdata

admin.site.register(swablogdata)
admin.site.register(swabpatientdata)
admin.site.register(plasmalogdata)
admin.site.register(plasmapatientdata)