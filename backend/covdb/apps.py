from django.apps import AppConfig


class CovdbConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'covdb'
