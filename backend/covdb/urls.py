from django.urls import path
from . import views

urlpatterns=[
    path('',views.index, name='index'),
    path('index', views.index, name='index'),
    path('about', views.about, name='about'),
    path('team', views.team, name='team'),
    path('publications', views.publications, name='publications'),
    path('result', views.result, name='result'),
    path('patient', views.patient, name='patient'),
    path('search', views.search, name='search')

] 