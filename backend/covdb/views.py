from django.http import JsonResponse
from django.shortcuts import render
from .models import swablogdata
from .models import plasmalogdata
from .models import swabpatientdata #Importing the patient swab data
from .models import plasmapatientdata #Importing the patient plasma data
from plotly.offline import plot
import plotly.graph_objects as go


# Create your views here.

def index(request):
    return render(request, "index.html")

def about(request):
    return render(request, "about.html")

def team(request):
    return render(request, "team.html")

def publications(request):
    return render(request, "publications.html")

def result(request):
    search=request.GET['search']
    if len(search)>=2:
        datas = (swablogdata.objects.filter(protein_name__icontains=search) | swablogdata.objects.filter(uniprot_id__icontains=search) | swablogdata.objects.filter(protein_id__icontains=search) | swablogdata.objects.filter(gene_name__icontains=search))
        datap= (plasmalogdata.objects.filter(uniprot_id__icontains=search) | plasmalogdata.objects.filter(gene_name__icontains=search))
        

#if both plasma and swab data is present
        if len(datas)==1 and len(datap)==1:
            search="Let's explore "+ search

            #The data declared inside def cannot be pulled in another def.
            
            if len(datas):
                all_objects= datas.values('s155','s165','s167','s20','s23','s1','s15','s16','s17','s18','s19','s2','s204','s208','s22','s25','s26','s27','s28','s29','s32','s35','s12','s13','s11','s173','s175','s183','s184','s190','s192','s198','s206','s214','s221','s228','s30','s31','s37','s38','s39','s68','s69','s71')
                beta= all_objects.values_list('s155','s165','s167','s20','s23','s1','s15','s16','s17','s18','s19','s2','s204','s208','s22','s25','s26','s27','s28','s29','s32','s35','s12','s13','s11','s173','s175','s183','s184','s190','s192','s198','s206','s214','s221','s228','s30','s31','s37','s38','s39','s68','s69','s71')
                res1 = list(beta)
                res1= res1[0]
                res1=list(res1)
                res1=list(map(float, res1))
                texth= swabpatientdata.objects.values('age', 'sex', 'severe_clinical_symptoms','on_ventilation','outcome')
                texth=list(texth)
                #Converts queryset datatype into list which can be displayed in front-end.

                def swab_plot():
                    animals=['s155','s165','s167','s20','s23','s1','s15','s16','s17','s18','s19','s2','s204','s208','s22','s25','s26','s27','s28','s29','s32','s35','s12','s13','s11','s173','s175','s183','s184','s190','s192','s198','s206','s214','s221','s228','s30','s31','s37','s38','s39','s68','s69','s71']
                    fig = go.Figure([go.Bar(x=animals, y= res1)])
                    fig.update_xaxes(tickvals=animals)
                
                    fig.update_layout(uniformtext_minsize=3, uniformtext_mode='show', xaxis_title= "Sample ids", yaxis_title= "Log 2 intensity")
                    #uniformtext_minsize is the text size on top of bar, if we don't pecify it the text size will be automatically determined. uniformtext_mode="hide" will not show the text if the textsizw will be smaller than minsize due to the number of columns, "show" will show them irrespective of the overlap.
                    fig.update_traces(marker_color= ["red","red","red","red","red","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green"
])                
                    fig.update_traces(text=res1, selector=dict(type='bar'))
                    #Sets text elements associated with each (x,y) pair. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates.
                    fig.update_traces(textposition='outside')
                    fig.update_traces(hovertext=texth, selector=dict(type='bar'))
                    #Sets hover text elements associated with each (x,y) pair. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates.              
                    fig.update_layout(margin=dict(l=10, r=10, t=40, b=50))
                    #Puts white margin arounf the plot for space to write text, labels.
                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

                def swabbox_plot():
                    y0 = res1[0:5]
                    y1 = res1[5:22]
                    y2= res1[22:]

                    fig = go.Figure()
                    fig.add_trace(go.Box(y=y0, name="Severe", marker_color = 'black', line_color="red"))
                    fig.add_trace(go.Box(y=y1, name="Non-severe", marker_color = 'black', line_color="gold"))
                    fig.add_trace(go.Box(y=y2, name="Healthy", marker_color = 'black', line_color="green"))
                    fig.update_traces(boxmean= 'sd', selector=dict(type='box'))
                    fig.update_traces(showlegend=False, selector=dict(type='box'))
                
                    fig.update_layout(margin=dict(l=10, r=10, t=10, b=10))
                    fig.update_traces(boxpoints='all',jitter=0, pointpos=0)

                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

            if len(datap):
            ##Plasma data-
                all_objectp= datap.values('p102','p105','p112',	'p116',	'p118',	'p119',	'p122',	'p125',	'p129',	'p132',	'p135',	'p136',	'p148',	'p25',	'p27',	'p50',	'p65',	'p66',	'p68',	'p94',	'p101',	'p104',	'p113',	'p114',	'p115',	'p2',	'p26',	'p28',	'p30',	'p48',	'p77',	'p81',	'p85',	'p86',	'p88',	'p93',	'p96',	'p99',	'p10',	'p103',	'p106',	'p11',	'p12',	'p120',	'p121',	'p124',	'p13',	'p14',	'p145',	'p146',	'p16',	'p17',	'p22',	'p3',	'p31',	'p4',	'p43',	'p45',	'p46',	'p49',	'p5',	'p51',	'p54',	'p6',	'p62',	'p73',	'p78',	'p79',	'p87',	'p9',	'p98')
                betap= all_objectp.values_list('p102',	'p105',	'p112',	'p116',	'p118',	'p119',	'p122',	'p125',	'p129',	'p132',	'p135',	'p136',	'p148',	'p25',	'p27',	'p50',	'p65',	'p66',	'p68',	'p94',	'p101',	'p104',	'p113',	'p114',	'p115',	'p2',	'p26',	'p28',	'p30',	'p48',	'p77',	'p81',	'p85',	'p86',	'p88',	'p93',	'p96',	'p99',	'p10',	'p103',	'p106',	'p11',	'p12',	'p120',	'p121',	'p124',	'p13',	'p14',	'p145',	'p146',	'p16',	'p17',	'p22',	'p3',	'p31',	'p4',	'p43',	'p45',	'p46',	'p49',	'p5',	'p51',	'p54',	'p6',	'p62',	'p73',	'p78',	'p79',	'p87',	'p9',	'p98')
                res2 = list(betap)
                res2= res2[0]
                res2=list(res2)
                res2=list(map(float, res2))
                textp= plasmapatientdata.objects.values('age', 'sex', 'severe_clinical_symptoms','on_ventilation','outcome')
                textp=list(textp)

                def plasma_plot():
                    boars=['p102',	'p105',	'p112',	'p116',	'p118',	'p119',	'p122',	'p125',	'p129',	'p132',	'p135',	'p136',	'p148',	'p25',	'p27',	'p50',	'p65',	'p66',	'p68',	'p94',	'p101',	'p104',	'p113',	'p114',	'p115',	'p2',	'p26',	'p28',	'p30',	'p48',	'p77',	'p81',	'p85',	'p86',	'p88',	'p93',	'p96',	'p99',	'p10',	'p103',	'p106',	'p11',	'p12',	'p120',	'p121',	'p124',	'p13',	'p14',	'p145',	'p146',	'p16',	'p17',	'p22',	'p3',	'p31',	'p4',	'p43',	'p45',	'p46',	'p49',	'p5',	'p51',	'p54',	'p6',	'p62',	'p73',	'p78',	'p79',	'p87',	'p9',	'p98']
                    fig = go.Figure([go.Bar(x=boars, y= res2)])
                    fig.update_xaxes(tickvals=boars)
                
                    fig.update_layout(xaxis_title= "Sample ids", yaxis_title= "Log 2 intensity")
                    fig.update_traces(marker_color= ["green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green", "yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow", "red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red", "red"
])
                    fig.update_traces(selector=dict(type='bar'))
                    fig.update_traces(textposition='outside')
                    fig.update_traces(hovertext=textp, selector=dict(type='bar'))                
                    fig.update_layout(margin=dict(l=10, r=10, t=40, b=50))
                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

                def plasmabox_plot():
                    y0 = res2[0:20]
                    y1 = res2[20:38]
                    y2= res2[38:]

                    fig = go.Figure()
                    fig.add_trace(go.Box(y=y2, name="Severe", marker_color = 'black', line_color="Red"))
                    fig.add_trace(go.Box(y=y1, name="Non-severe", marker_color = 'black', line_color="gold"))
                    fig.add_trace(go.Box(y=y0, name="Healthy", marker_color = 'black', line_color="Green"))
                    fig.update_traces(boxmean= 'sd', selector=dict(type='box'))
                    fig.update_traces(showlegend=False, selector=dict(type='box'))
                    fig.update_layout(margin=dict(l=10, r=10, t=10, b=10))
                    fig.update_traces(boxpoints='all',jitter=0, pointpos=0)
                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

            return render(request, 'result.html',{'search':search, 'datas':datas, 'splot':swab_plot(),'swabbox':swabbox_plot(), 'pplot':plasma_plot, 'plasmabox':plasmabox_plot,  "swabdata":"Swab data","swabboxplot":"Box plot", "plasmadata":"Plasma data","plasmaboxplot":"Box plot"})


#if only swab data is present
        elif len(datas)==1:
            search="Let's explore "+ search

            #The data declared inside def cannot be pulled in another def.
            
            if len(datas):
                all_objects= datas.values('s155','s165','s167','s20','s23','s1','s15','s16','s17','s18','s19','s2','s204','s208','s22','s25','s26','s27','s28','s29','s32','s35','s12','s13','s11','s173','s175','s183','s184','s190','s192','s198','s206','s214','s221','s228','s30','s31','s37','s38','s39','s68','s69','s71')
                beta= all_objects.values_list('s155','s165','s167','s20','s23','s1','s15','s16','s17','s18','s19','s2','s204','s208','s22','s25','s26','s27','s28','s29','s32','s35','s12','s13','s11','s173','s175','s183','s184','s190','s192','s198','s206','s214','s221','s228','s30','s31','s37','s38','s39','s68','s69','s71')
                res1 = list(beta)
                res1= res1[0]
                res1=list(res1)
                res1=list(map(float, res1))
                texth= swabpatientdata.objects.values('age', 'sex', 'severe_clinical_symptoms','on_ventilation','outcome')
                texth=list(texth)
                #Converts queryset datatype into list which can be displayed in front-end.

                def swab_plot():
                    animals=['s155','s165','s167','s20','s23','s1','s15','s16','s17','s18','s19','s2','s204','s208','s22','s25','s26','s27','s28','s29','s32','s35','s12','s13','s11','s173','s175','s183','s184','s190','s192','s198','s206','s214','s221','s228','s30','s31','s37','s38','s39','s68','s69','s71']
                    fig = go.Figure([go.Bar(x=animals, y= res1)])
                    fig.update_xaxes(tickvals=animals)
                
                    fig.update_layout(uniformtext_minsize=3, uniformtext_mode='show', xaxis_title= "Sample ids", yaxis_title= "Log 2 intensity")
                    #uniformtext_minsize is the text size on top of bar, if we don't pecify it the text size will be automatically determined. uniformtext_mode="hide" will not show the text if the textsizw will be smaller than minsize due to the number of columns, "show" will show them irrespective of the overlap.
                    fig.update_traces(marker_color= ["red","red","red","red","red","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green"
])                
                    fig.update_traces(text=res1, selector=dict(type='bar'))
                    #Sets text elements associated with each (x,y) pair. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates.
                    fig.update_traces(textposition='outside')
                    fig.update_traces(hovertext=texth, selector=dict(type='bar'))
                    #Sets hover text elements associated with each (x,y) pair. If a single string, the same string appears over all the data points. If an array of string, the items are mapped in order to the this trace's (x,y) coordinates.              
                    fig.update_layout(margin=dict(l=10, r=10, t=40, b=50))
                    #Puts white margin arounf the plot for space to write text, labels.
                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

                def swabbox_plot():
                    y0 = res1[0:5]
                    y1 = res1[5:22]
                    y2= res1[22:]

                    fig = go.Figure()
                    fig.add_trace(go.Box(y=y0, name="Severe", marker_color = 'black', line_color="red"))
                    fig.add_trace(go.Box(y=y1, name="Non-severe", marker_color = 'black', line_color="gold"))
                    fig.add_trace(go.Box(y=y2, name="Healthy", marker_color = 'black', line_color="green"))
                    fig.update_traces(boxmean= 'sd', selector=dict(type='box'))
                    fig.update_traces(showlegend=False, selector=dict(type='box'))
                
                    fig.update_layout(margin=dict(l=10, r=10, t=10, b=10))
                    fig.update_traces(boxpoints='all',jitter=0, pointpos=0)

                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

                return render(request, 'result.html',{'search':search, 'datas':datas, 'splot':swab_plot(),'swabbox':swabbox_plot(),    "swabdata":"Swab data","swabboxplot":"Box plot","plasmadata":"Sorry but we dont have plasma data for this protein","plasmaboxplot":""})


#if only plasma data is present
        elif len(datap)==1:
            search="Let's explore "+ search

            if len(datap):
            ##Plasma data-
                all_objectp= datap.values('p102','p105','p112',	'p116',	'p118',	'p119',	'p122',	'p125',	'p129',	'p132',	'p135',	'p136',	'p148',	'p25',	'p27',	'p50',	'p65',	'p66',	'p68',	'p94',	'p101',	'p104',	'p113',	'p114',	'p115',	'p2',	'p26',	'p28',	'p30',	'p48',	'p77',	'p81',	'p85',	'p86',	'p88',	'p93',	'p96',	'p99',	'p10',	'p103',	'p106',	'p11',	'p12',	'p120',	'p121',	'p124',	'p13',	'p14',	'p145',	'p146',	'p16',	'p17',	'p22',	'p3',	'p31',	'p4',	'p43',	'p45',	'p46',	'p49',	'p5',	'p51',	'p54',	'p6',	'p62',	'p73',	'p78',	'p79',	'p87',	'p9',	'p98')
                betap= all_objectp.values_list('p102',	'p105',	'p112',	'p116',	'p118',	'p119',	'p122',	'p125',	'p129',	'p132',	'p135',	'p136',	'p148',	'p25',	'p27',	'p50',	'p65',	'p66',	'p68',	'p94',	'p101',	'p104',	'p113',	'p114',	'p115',	'p2',	'p26',	'p28',	'p30',	'p48',	'p77',	'p81',	'p85',	'p86',	'p88',	'p93',	'p96',	'p99',	'p10',	'p103',	'p106',	'p11',	'p12',	'p120',	'p121',	'p124',	'p13',	'p14',	'p145',	'p146',	'p16',	'p17',	'p22',	'p3',	'p31',	'p4',	'p43',	'p45',	'p46',	'p49',	'p5',	'p51',	'p54',	'p6',	'p62',	'p73',	'p78',	'p79',	'p87',	'p9',	'p98')
                res2 = list(betap)
                res2= res2[0]
                res2=list(res2)
                res2=list(map(float, res2))
                textp= plasmapatientdata.objects.values('age', 'sex', 'severe_clinical_symptoms','on_ventilation','outcome')
                textp=list(textp)

                def plasma_plot():
                    boars=['p102',	'p105',	'p112',	'p116',	'p118',	'p119',	'p122',	'p125',	'p129',	'p132',	'p135',	'p136',	'p148',	'p25',	'p27',	'p50',	'p65',	'p66',	'p68',	'p94',	'p101',	'p104',	'p113',	'p114',	'p115',	'p2',	'p26',	'p28',	'p30',	'p48',	'p77',	'p81',	'p85',	'p86',	'p88',	'p93',	'p96',	'p99',	'p10',	'p103',	'p106',	'p11',	'p12',	'p120',	'p121',	'p124',	'p13',	'p14',	'p145',	'p146',	'p16',	'p17',	'p22',	'p3',	'p31',	'p4',	'p43',	'p45',	'p46',	'p49',	'p5',	'p51',	'p54',	'p6',	'p62',	'p73',	'p78',	'p79',	'p87',	'p9',	'p98']
                    fig = go.Figure([go.Bar(x=boars, y= res2)])
                    fig.update_xaxes(tickvals=boars)
                
                    fig.update_layout(xaxis_title= "Sample ids", yaxis_title= "Log 2 intensity")
                    fig.update_traces(marker_color= ["green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green","green", "yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow", "red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red","red", "red"
])
                    fig.update_traces(selector=dict(type='bar'))
                    fig.update_traces(textposition='outside')
                    fig.update_traces(hovertext=textp, selector=dict(type='bar'))                
                    fig.update_layout(margin=dict(l=10, r=10, t=40, b=50))
                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

                def plasmabox_plot():
                    y0 = res2[0:20]
                    y1 = res2[20:38]
                    y2= res2[38:]

                    fig = go.Figure()
                    fig.add_trace(go.Box(y=y2, name="Severe", marker_color = 'black', line_color="Red"))
                    fig.add_trace(go.Box(y=y1, name="Non-severe", marker_color = 'black', line_color="gold"))
                    fig.add_trace(go.Box(y=y0, name="Healthy", marker_color = 'black', line_color="Green"))
                    fig.update_traces(boxmean= 'sd', selector=dict(type='box'))
                    fig.update_traces(showlegend=False, selector=dict(type='box'))
                    fig.update_layout(margin=dict(l=10, r=10, t=10, b=10))
                    fig.update_traces(boxpoints='all',jitter=0, pointpos=0)
                    plot_div = plot(fig, output_type='div', include_plotlyjs=False)
                    return plot_div

            return render(request, 'result.html',{'search':search, 'datas':datap, 'pplot':plasma_plot, 'plasmabox':plasmabox_plot,  "swabdata":"Sorry but we dont have Swab data for this protein. check out Plasma data.","swabboxplot":"", "plasmadata":"Plasma data","plasmaboxplot":"Box plot"})


#for table generation
        elif datas or datap:
            return render(request, "searchresult.html",{"search":search, "datas":datas, "datap":datap})
        
        else:
            return render(request, "result.html",{"search":"Sorry but the protein you are looking for isn't found here"} )
            
    else: 
        return render(request, "result.html",{"search":"Please try to be more specific or enter something."} )




 #search API

def search(request):
    search=request.GET.get('search')
    
    ids = []

    if search:
        datas = (swablogdata.objects.filter(protein_name__icontains=search) | swablogdata.objects.filter(uniprot_id__icontains=search) | swablogdata.objects.filter(protein_id__icontains=search) | swablogdata.objects.filter(gene_name__icontains=search))

        for data in datas:
            ids.append(data.protein_id)
    
    return JsonResponse({'status' : 200 , 'id' : ids})




def patient(request):
    swabdatas = swabpatientdata.objects.all()
    plasmadatas = plasmapatientdata.objects.all()

    return render(request, "patient.html",{"swabdatas": swabdatas , "plasmadatas": plasmadatas})
   