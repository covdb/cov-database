CREATE TABLE IF NOT EXISTS covdb_plasmapatientdata(
   patient_id               VARCHAR(3)
  ,grouping                 VARCHAR(10)
  ,status                   VARCHAR(8)
  ,age                      INTEGER 
  ,sex                      VARCHAR(1)
  ,ct                       VARCHAR(12)
  ,severe_clinical_symptoms VARCHAR(93)
  ,on_ventilation           VARCHAR(3)
  ,outcome                  VARCHAR(10)
);
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('25','Negative','Negative',50,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('27','Negative','Negative',30,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('50','Negative','Negative',18,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('65','Negative','Negative',29,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('66','Negative','Negative',75,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('68','Negative','Negative',23,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('94','Negative','Negative',20,'M','NA','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('102','Negative','Negative',28,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('105','Negative','Negative',49,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('112','Negative','Negative',55,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('116','Negative','Negative',37,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('118','Negative','Negative',35,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('119','Negative','Negative',24,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('122','Negative','Negative',86,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('125','Negative','Negative',76,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('129','Negative','Negative',20,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('132','Negative','Negative',25,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('135','Negative','Negative',38,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('136','Negative','Negative',38,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('148','Negative','Negative',32,'M','N/A','No','no','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('3','Severe','Positive',61,'F','29/29','ARDS, Pneumonia','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('4','Severe','Positive',68,'M','36/36','Pneumonia, ARDS','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('5','Severe','Positive',56,'M','35/34','ARDS, Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('6','Severe','Positive',25,'M','34/36','B/L Crepts, ARDS, Pneumonia','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('9','Severe','Positive',34,'M','26/26','Cardiac Fibro-bronchitis(cause of death) , B/L Pneumonia','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('10','Severe','Positive',73,'M','29/28','GGO, Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('11','Severe','Positive',43,'M','23/22','B/l Crepts, ARDS, B/l GGO','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('12','Severe','Positive',57,'M','36/34','Type-1 respiratory failure Acute respiratory distress syndrome with covid-19 (cause of death)','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('13','Severe','Positive',70,'M','32/32','B/l crepts, Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('14','Severe','Positive',57,'M','30/28','Pneumonia, B/L crepts','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('16','Severe','Positive',68,'M','22/21','ARDS, Pneumonia, B/l Crepti, Lung infection(cause of death)','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('17','Severe','Positive',60,'F','18/18','B/l GGO, ARDS, B/l crepts','Yes','ischarged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('22','Severe','Positive',55,'M','28/30','B/l crepti, ARDS(cause of death)','yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('31','Severe','Positive',50,'M','N/A','ARDS(cause of death) Pneumonia, B/l GGO','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('43','Severe','Positive',63,'M','30/30','B/l GGO, ARDS(cause of death) B/l crepts, B/l Pneumonia','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('45','Severe','Positive',70,'F','Antigen test','ARDS(cause of death)','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('46','Severe','Positive',58,'M','N/A','B/l Crepts, Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('49','Severe','Positive',50,'M','30/29','Pneumona, B/L crepts','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('51','Severe','Positive',58,'M','26/25','B/l Pneumonia, ARDS (cause of death)','Yes','Dead');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('54','Severe','Positive',40,'M','32/31','ARDS','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('62','Severe','Positive',57,'M','32/33','ARDS B/l Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('106','Severe','Positive',70,'F','Antigen test','B/L Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('73','Severe','Positive',39,'M','22/21','B/l Pneumonia, ARDS (cause of death)','Yes','Death');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('78','Severe','Positive',53,'F','25/25','Yes,ARI','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('79','Severe','Positive',58,'F','20/22','B/L Pneumonia, ARDS','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('87','Severe','Positive',72,'F','22/22','Yes, Acute Respiratory Distress Syndrome (cause of death)','Yes','Dead');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('98','Severe','Positive',67,'M','16/18','B/l Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('103','Severe','Positive',90,'M','N/A','Yes, Acute Respiratory Distress Syndrome (cause of death)','Yes','Dead');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('120','Severe','Positive',36,'M','Antigen test','Yes, ARI','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('121','Severe','Positive',70,'M','26/22','IL-6 high','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('124','Severe','Positive',30,'M','34/34','IL-6 high, ARDS, B/l Pneumonia','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('145','Severe','Positive',32,'F','23/32/40','Yes, ARI','Yes','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('146','Severe','Positive',56,'M','30/35','Yes, Acute Respiratory Distress Syndrome (cause of death)','Yes','Dead');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('2','Non-Severe','Positive',61,'M','27/26','No','No','discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('26','Non-Severe','Positive',60,'F','33/33','No','No','discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('28','Non-Severe','Positive',53,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('30','Non-severe','Positive',68,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('48','Non-Severe','Positive',57,'M','34/32','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('77','Non-Severe','Positive',56,'M','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('81','Non-severe','Positive',59,'F','Antigen test','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('85','Non-Severe','Positive',53,'F','N/A','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('86','Non-severe','Positive',74,'M','24/24','Pneumonia mild','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('88','Non-Severe','Positive',57,'M','37/37','No','No','Discharged');
INSERT INTO covdb_plasmapatientdata(patient_id,grouping,status,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES ('93','Non-Severe','Positive',60,'F','17/18','No','No','Discharged');
