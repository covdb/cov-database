CREATE TABLE IF NOT EXISTS covdb_swabpatientdata(
   patient_id               INTEGER  NOT NULL PRIMARY KEY 
  ,status                   VARCHAR(8) NOT NULL
  ,grouping                 VARCHAR(10) NOT NULL
  ,age                      VARCHAR(3) NOT NULL
  ,sex                      VARCHAR(1)
  ,ct                       VARCHAR(20) NOT NULL
  ,severe_clinical_symptoms VARCHAR(21) NOT NULL
  ,on_ventilation           VARCHAR(3) NOT NULL
  ,outcome                  VARCHAR(29) NOT NULL
);
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (155,'Positive','Severe','65','M','16\16','ARDS B/L Pneumonitis','Yes','DAMA');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (165,'Positive','Severe','65','F','23\24','ARDS','Yes','Death');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (20,'Positive','Severe','76','F','16\16','ARDS, Pneumonia','Yes','Transferred to other hospital');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (23,'Positive','Severe','49','M','36\36','B/l Crepts, Pneumonia','Yes','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (167,'Positive','Severe','33','M','22\26','B/l Crepts, Pneumonia','Yes','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (2,'Positive','Non-severe','49','F','16\15','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (16,'Positive','Non-severe','28','M','34\34','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (17,'Positive','Non-severe','22','M','28\28','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (18,'Positive','Non-severe','63','M','30\30','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (19,'Positive','Non-severe','31','F','32\32','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (22,'Positive','Non-severe','58','M','29\29','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (25,'Positive','Non-severe','67','F','29\28','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (26,'Positive','Non-severe','32','M','24\24','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (27,'Positive','Non-severe','29','F','27\32','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (28,'Positive','Non-severe','61','M','30\32','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (29,'Positive','Non-severe','43','F','28\29','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (32,'Positive','Non-severe','65','M','28\29','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (35,'Positive','Non-severe','60','F','36\36','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (1,'Positive','Non-severe','35','F','19\19','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (15,'Positive','Non-severe','46','F','21\21','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (208,'Positive','Non-severe','60','M','24\26','No','No','DAMA');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (204,'Positive','Non-severe','46','M','20\24','No','Yes','DAMA');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (37,'Negative','Recovered','60','M','Negative (Recovered)','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (38,'Negative','Non-severe','19','F','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (39,'Negative','Recovered','45','F','Negative (Recovered)','N/A','N/A','N/A');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (68,'Negative','Recovered','48','M','Negative (Recovered)','N/A','N/A','N/A');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (69,'Negative','Recovered','60','M','Negative (Recovered)','N/A','N/A','N/A');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (71,'Negative','Recovered','49','M','Negative (Recovered)','N/A','N/A','N/A');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (175,'Negative','Negative','63','F','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (183,'Negative','Negative','36','M','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (190,'Negative','Negative','38','F','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (192,'Negative','Negative','42','M','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (214,'Negative','Negative','39','M','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (221,'Negative','Negative','53','M','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (228,'Negative','Negative','47','M','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (173,'Negative','Non-severe','45','F','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (184,'Negative','Non-severe','33','M','True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (198,'Negative','Non-severe','40',NULL,'True Negative','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (206,'Negative','Negative','75','F','True Negative','B/L Pneumonia','Yes','DAMA');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (11,'Negative','Recovered','78','M','Negative (Recovered)','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (12,'Negative','Recovered','51','M','Negative (Recovered)','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (13,'Negative','Recovered','50','M','Negative (Recovered)','No','No','Discharged');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (30,'Negative','Negative','N/A',NULL,'Negative','N/A','N/A','N/A');
INSERT INTO covdb_swabpatientdata(patient_id,status,grouping,age,sex,ct,severe_clinical_symptoms,on_ventilation,outcome) VALUES (31,'Negative','Negative','N/A',NULL,'Negative','N/A','N/A','N/A');
